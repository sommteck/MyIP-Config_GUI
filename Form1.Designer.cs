namespace MyIP_Config_GUI
{
    partial class MyIP_Config
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_show = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.lst_output = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // cmd_show
            // 
            this.cmd_show.Location = new System.Drawing.Point(38, 237);
            this.cmd_show.Name = "cmd_show";
            this.cmd_show.Size = new System.Drawing.Size(102, 42);
            this.cmd_show.TabIndex = 0;
            this.cmd_show.Text = "&Configuration anzeigen";
            this.cmd_show.UseVisualStyleBackColor = true;
            this.cmd_show.Click += new System.EventHandler(this.cmd_show_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(239, 237);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 1;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // lst_output
            // 
            this.lst_output.FormattingEnabled = true;
            this.lst_output.ItemHeight = 16;
            this.lst_output.Location = new System.Drawing.Point(62, 39);
            this.lst_output.Name = "lst_output";
            this.lst_output.Size = new System.Drawing.Size(157, 132);
            this.lst_output.TabIndex = 2;
            // 
            // MyIP_Config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 322);
            this.Controls.Add(this.lst_output);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_show);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MyIP_Config";
            this.Text = "My IP-Config";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmd_show;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.ListBox lst_output;
    }
}

