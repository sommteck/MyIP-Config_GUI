using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;           // Für Netz-Klassen; z.B. für Domainenaktionen

namespace MyIP_Config_GUI
{
    public partial class MyIP_Config : Form
    {
        public MyIP_Config()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_show_Click(object sender, EventArgs e)
        {
            lst_output.Items.Clear();
            lst_output.Items.Add("Rechnername: " + Dns.GetHostName());  // Anzeige des Computernamens
            lst_output.Items.Add("IP-Adresse: ");
            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                lst_output.Items.Add(ip);
        }
    }
}
